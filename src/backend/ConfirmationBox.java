package backend;

import java.io.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.stage.*;

/**
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class ConfirmationBox {
	boolean answer = false;
	
	/**
	 * Prompts the user with a confirmation box for deleting objects
	 */
	public boolean confirmationAlert () throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/Confirmation.fxml"));
		Parent root = (Parent)loader.load();
		Stage deleteWindow = new Stage();
		
		Scene scene = new Scene(root);
		deleteWindow.setScene(scene);
		deleteWindow.setResizable(false);
		
		Button yesButton = (Button) scene.lookup("#yesButton");
		Button noButton = (Button) scene.lookup("#noButton");
		
		yesButton.setOnAction(e -> {
			answer = true;
			deleteWindow.close();
		});
		noButton.setOnAction(e -> {
			answer = false;
			deleteWindow.close();
		});
		deleteWindow.showAndWait();
		return answer;
	}
	
}
