package backend;

import java.util.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

/**
 * This class is used by the various controller objects needed
 * for the various stages/windows.
 * @author andrewgonzalez
 * @author danielrodriguez
 */
public class Controller {

	public Stage primary;
	public User user;
	public ArrayList<Photo> photos;
	
	public void start(Stage primary, User user, ArrayList<Photo> photos){
		this.primary = primary;
		this.user = user;
		this.photos = photos;
		return;
	}
	
	
	public void start(Stage primary, User user){
		this.primary = primary;
		this.user = user;
		return;
	}
	
	public void start (Stage primary){
		this.primary = primary;
	}
	
	public void closeWindow(){
		primary.close();
	}
	//creditential 
	/** Prompts the User that an error occurred
	 */
	public void error(String message){
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(this.primary);
		alert.setTitle("ALERT ERROR");
		alert.setHeaderText("Error: Invalid Credentials");
		alert.setContentText(message);
		alert.showAndWait();
	}
	
	
}
