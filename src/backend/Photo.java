package backend;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date; 

/** Photos object class
 * @author andrewgonzalez
 * @author danielrodriguez
 */

public class Photo implements Serializable {

		String caption =  null;
		File file;
		Date date;
		ArrayList<Tag> tags;
		private static final long serialVersionUID = 1L;
		
		
	public Photo (File file, String caption){
			this.caption = caption;
			this.file = file;
			this.date = new Date(file.lastModified());
			this.tags = new ArrayList<Tag>();
		}
	
	public Photo(File file){
		this.caption = file.getPath();
		this.file = file;
		this.date = new Date(file.lastModified());
		this.tags = new ArrayList<Tag>();
	}
	
	public Photo(){
		
	}
	
	/** Changes the caption of the photo
	 */
	public void setCaption(String caption){
		this.caption = caption;
	}
	
	/** Used to add tags to the list of tags
	 */
	public void addTag(Tag t){
		this.tags.add(t);
	}
	
	/** Used to remove tags to the list of tags
	 */
	public void deleteTag(Tag t){
		//check if tag is in the list, if it is remove it, else do nothing:
		if(this.tags.contains(t)){
			this.tags.remove(t);
		}
	}
	
	/** Used to check if tag exist in the photo's tag list
	 */
	public boolean containsTag(Tag t){
		for (Tag i : this.tags){
			if(i.equals(t)){
				return true;
			}
		}
		return false;
	}
	
	/** returns the photo's caption
	 */
	public String getCaption(){
		return this.caption;
	}
	
	/** returns the file of the photo
	 */
	public File getFile(){
		return this.file;
	}
	/** returns the Date of the photo
	 */
	public Date getDate(){
		return this.date;
	}
	
	/** Puts the date into a readable String format of mm/dd/yy
	 */
	public String getDateString(){
		
		return new SimpleDateFormat("MM/dd/yy").format(this.getDate());
	}
	
	/** returns the list of tags in the photo
	 */
	public ArrayList<Tag> getTags(){
		return this.tags;
	}
	
	/** returns the number of Tags
	 */
	public int getNumTags(){
		return this.tags.size();
	}
	
	/** returns the path of the photo
	 */
	public String getPath(){
		return this.file.getPath();
	}
	
	public String toString() {
		return this.caption;
	}
}
