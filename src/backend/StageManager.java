package backend;

import java.io.*;
import java.util.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.stage.*;

import controllers.AddPhotoController;
import controllers.AddTagController;
import controllers.CopyPhotoController;
import controllers.EditCaptionController;
import controllers.MovePhotoController;
import controllers.PhotosController;

/**
 * 
 * @author andrewgonzalez
 *@author danielrodriguez
 */

public class StageManager {
	/**
	 * Given a scene name and a user this will create a stage using the given name for
	 * the currently signed in user.
	 */
	public Stage getStage(String sceneName, User user) throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/" + sceneName +".fxml"));
		Parent root = (Parent)loader.load();
		Controller controller = loader.getController();
				
		Stage secondaryStage = new Stage();
		controller.start(secondaryStage,user);
								
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false);
				
		return secondaryStage;
	}
	
	public Stage getStage (String sceneName, User user, ArrayList<Photo> result) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/" + sceneName +".fxml"));
		Parent root = (Parent)loader.load();
		Controller controller = loader.getController();
				
		Stage secondaryStage = new Stage();
		controller.start(secondaryStage,user,result);
								
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false);
				
		return secondaryStage;
	}
	
	public Stage getStage (String sceneName) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/" + sceneName +".fxml"));
		Parent root = (Parent)loader.load();
		Controller controller = loader.getController();
				
		Stage secondaryStage = new Stage();
		controller.start(secondaryStage);
								
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false);
				
		return secondaryStage;
	}
	
	/**
	 * When adding a photo to the current user's an album this will create a stage\
	 */
	
	public Stage getAddPhotoStage (User current, Album album) throws IOException{
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/AddPhoto.fxml"));
		Parent root = (Parent)loader.load();
		AddPhotoController addPhotoController = loader.getController();
		
		Stage secondaryStage = new Stage();
		addPhotoController.start(secondaryStage, current, album);
		
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false); 
		
		return secondaryStage;
	}
	
	/**
	 * Loads the scene given ('sceneName') on top of the given primary stage given ('primary')
	 * along with the user being passed.
	 */
	
	public void loadScene (Stage primary, String sceneName,User user) throws IOException {
		// Set up loginController
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/" + sceneName + ".fxml"));
		Parent root = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.start(primary,user);
				
		// Set up primaryStage
		Scene scene = new Scene(root);
		primary.setScene(scene);
		primary.setTitle("Photo App");
		primary.setResizable(false); 
		primary.show();		
	}
	
	public void loadScene (Stage primary, String sceneName) throws IOException {
		// Set up loginController
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/" + sceneName + ".fxml"));
		Parent root = (Parent)loader.load();
		Controller controller = loader.getController();
		controller.start(primary);
				
		// Set up primaryStage
		Scene scene = new Scene(root);
		primary.setScene(scene);
		primary.setTitle("Photo App");
		primary.setResizable(false); 
		primary.show();		
	}
	
	/**
	 * Loads the Photos on top of the primary stage given as 'primary'
	 */
	
	public void loadPhotosScene (Stage primary, User current, Album selectedAlbum) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/PhotosGUI.fxml"));
		Parent root = (Parent)loader.load();
		PhotosController photosController = loader.getController();
		photosController.start(primary, current, selectedAlbum);
		
		Scene scene = new Scene(root);
		primary.setScene(scene);
		primary.setTitle("Photo App");
		primary.setResizable(false); 
		primary.show();
	}
	
	
	/**
	 * Creates a stage for when copying photos 
	 */
	public Stage getCopyPhotoStage(User current, Album album, Photo selectedPhoto) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/CopyPhoto.fxml"));
		Parent root = (Parent)loader.load();
		CopyPhotoController copyPhotoController = loader.getController();
		
		Stage secondaryStage = new Stage();
		copyPhotoController.start(secondaryStage, current, album, selectedPhoto);
		
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false); 
		
		return secondaryStage;
	}
	
	
	/**
	 * Creates a stage for when moving photos
	 */
	
	public Stage getMovePhotoStage(User current, Album album, Photo selectedPhoto) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/MovePhoto.fxml"));
		Parent root = (Parent)loader.load();
		MovePhotoController movePhotoController = loader.getController();
		
		Stage secondaryStage = new Stage();
		movePhotoController.start(secondaryStage, current, album, selectedPhoto);
		
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false); 
		
		return secondaryStage;
	}
	
	/**
	 * Creates an edit caption stage based on the edit_captions scene
	 */
	public Stage getEditCaptionStage(User current, Photo selectedPhoto) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/EditCaption.fxml"));
		Parent root = (Parent)loader.load();
		EditCaptionController editCaptionController = loader.getController();
		
		Stage secondaryStage = new Stage();
		editCaptionController.start(secondaryStage, current, selectedPhoto);
		
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false); 
		
		return secondaryStage;
	}	
	
	
	
	/**
	 * Creates a stage using the add_tage scene 
	 */
	
	public Stage getAddTagStage(User current, Photo selectedPhoto) throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/AddTag.fxml"));
		Parent root = (Parent)loader.load();
		AddTagController addTagController = loader.getController();
		
		Stage secondaryStage = new Stage();
		addTagController.start(secondaryStage, current, selectedPhoto);
		
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false); 
		
		return secondaryStage;
	}
	
	/**
	 * Prompts the user with confirmation Stage/confirmation box to confirm the action 
	 */
	public boolean getConfirmation () throws IOException {
		ConfirmationBox c = new ConfirmationBox();
		return c.confirmationAlert();
	}
	

}
