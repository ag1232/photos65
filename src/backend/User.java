package backend;
import java.io.*;
import java.util.*;


public class User implements Serializable {
	/**
	 * Implementation of a User object
	 */
	String userName = null;
	String password = null;
	String accountType = null;
	ArrayList<Album> albums;
	private static final long serialVersionUID = 1L;
	
	public User (String userName, String password, String accountType){
		this.userName = userName;
		this.password = password;
		this.accountType = accountType;
		this.albums = new ArrayList<Album>();
	}
	/** Function returns the user's name
	 */
	public String getUserName(){
		return userName;
		
	}
	/** Returns the user's password
	 */
	public String getPassword(){
		return password;
	}
	/** Returns the user's account type
	 */
	public String getAccountType(){
		return accountType;
	}
	/** Adds an album to the user's list of albums
	 */
	public void addAlbum(Album a){
		this.albums.add(a);
	}
	/** Removes an album for the user's list of albums
	 */
	public void deleteAlbum(Album a){
		//may have to do a try-catch here just in case it throws a null error
		this.albums.remove(a);
	}
	/** Returns the user's albums
	 */
	public ArrayList<Album> getAlbums(){
		return this.albums;
	}
	
	/** Used to save the user in an array list of stored users
	 */
	public void saveUser(){
		
		try {
			//Deserialize storedUsers data
			FileInputStream fileIn = new FileInputStream("accounts.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
			in.close();
			fileIn.close();
					
			//Traverses storedUsers and saves this User
			for (User u : storedUsers) {
				if (this.equals(u)) {
					storedUsers.set(storedUsers.indexOf(u), this);
				}
			}
					
			//Serialize updated storedUsers
			FileOutputStream fileOut = new FileOutputStream("accounts.dat");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(storedUsers);
			out.close();
			fileOut.close();
		}
		catch (ClassNotFoundException ex) {
			System.out.println("Class not found.");
		}
		catch (IOException ex) {
			System.out.println("Error reading file.");
		}
	}
	
	/** Used to compare users and their username + password
	 */
	public boolean equals(User user){
		if (!(user instanceof User) || user == null) return false;
		return this.userName.equals(user.getUserName()) && this.password.equals(user.password);
	}

}
