package controllers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import backend.Album;
import backend.Controller;
import backend.Photo;
import backend.User;

/** 
 * Controller for the add album stage
 * @author Andrew Gonzalez
 * @author Daniel Rodriguez
 */

public class AddAlbumController extends Controller{
	private User current;
	private ArrayList<Photo> result;
	@FXML TextField albumNameTextField;
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 * @param user is the user accessing stage
	 */
	public void start(Stage primary,User user) {
		this.current = user;
		this.primary = primary;
	}
	
	/**
	 * Initializes controller's private fields and sets up controller
	 * for stage
	 * @param primary is the stage attached to the controller
	 * @param user is the user accessing stage
	 * @param result is the arraylist of photos passed through AddAlbumController to fill the album
	 */	
	public void start(Stage primary,User user, ArrayList<Photo> result) {
		this.current = user;
		this.primary = primary;
		this.result = result;
		
	}
	

	 // If album name is valid, adds album to user
	
	 	public void addAlbum () {
		String albumName = albumNameTextField.getText();
		if (!albumName.isEmpty()) {
			for (Album album : current.getAlbums()) {
				if (album.getName().equals(albumName)) {
					error("Albums cannot have the same name.");
					return;
				}
			}
			
			if(result==null) {
				Album album = new Album(albumName);
				current.addAlbum(album);
			}else {
				Album album = new Album(albumName,result);
				current.addAlbum(album);
			}
			
			
			try {
				// Deserialize user data
				FileInputStream fileIn = new FileInputStream("accounts.dat");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
				in.close();
				fileIn.close();

				// Find the current user in storedUsers and adds valid album
				for (User user : storedUsers) {
					if (user.equals(current)) {
						storedUsers.set(storedUsers.indexOf(user), current);
					}
				}
				
				//Then have to re-serialize storedUsers
				FileOutputStream fileOut = new FileOutputStream("accounts.dat");
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(storedUsers);
				out.close();
				fileOut.close();
			}
			catch (ClassNotFoundException ex) {
			}
			catch (IOException ex) {
			}
		}
		else {
			return;
		}
		closeWindow();
	}
}
