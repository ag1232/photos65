package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import backend.Album;
import backend.Controller;
import backend.Photo;
import backend.User;

/** 
 * Controls the "Add_Photo" stage
 * @author Daniel Rodriguez
 * @author Andrew Gonzalez
 */
public class AddPhotoController extends Controller{
	private User current;
	private Album album;
	private File selectedFile;
	
	@FXML TextField photoPathTextField;
	@FXML TextField captionTextField;
	
	
	/**
	 * Initializes controller fields
	 * @param primaryStage is the stage attached to the controller
	 * @param user is the user accessing stage
	 * @param album is the album being passed to controller
	 */
	public void start (Stage primaryStage, User current, Album album) {
		this.primary = primaryStage;
		
		this.current = current;
		this.album = album;
	}
	
	 //user chooses a file, then populates selectedFile
	public void getUserFile () {
		FileChooser fileChooser = new FileChooser();
		selectedFile = fileChooser.showOpenDialog(primary);
		
		if(selectedFile == null) return;
		else{
			photoPathTextField.setText(selectedFile.getAbsolutePath());
		}

	}
	

	 // Depending on user's selectedFile, add photo to user album	 
	public void addPhoto () {
		if (selectedFile == null) {
			return;
		}
		if (selectedFile.exists()) {
			String caption = captionTextField.getText();
			if (!caption.isEmpty()) {
				album.addPhoto(new Photo (selectedFile, caption));
			}
			else {
				album.addPhoto(new Photo (selectedFile));
			}
			current.saveUser();
			closeWindow();
		}
	}
	
}

