package controllers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import backend.Album;
import backend.Controller;
import backend.Photo;
import backend.Tag;
import backend.User;

/** 
 * Controls the "Add_Tag" stage
 * @author Andrew Gonzalez
 * @author Daniel Rodriguez
 */
public class AddTagController extends Controller {
	private User current;
	private Photo selectedPhoto;
	
	@FXML TextField tagTypeTextField;
	@FXML TextField tagValueTextField;
	
	
	/**
	 * Initializes controller's private fields and sets up controller
	 * for stage
	 * @param primary is the Stage that this controller controls
	 * @param current is the user accessing the stage
	 * @param selectedPhoto is the Photo that the tag will be attached to
	 */
	public void start (Stage primary, User current, Photo selectedPhoto) {
		this.primary = primary;
		this.current = current;
		this.selectedPhoto = selectedPhoto;
	}
	
	//adds tag to selected photo
	public void addTag () {
		String tagType = tagTypeTextField.getText();
		String tagValue = tagValueTextField.getText();
		
		if (!tagType.isEmpty() && !tagValue.isEmpty()) {
			Tag tag = new Tag(tagType, tagValue);
			selectedPhoto.addTag(tag);
			current.saveUser();
			closeWindow();
		}
		else {
			error("Please provide input for both fields.");
		}
	}
	
}
