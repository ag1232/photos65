package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import backend.Controller;
import backend.User;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/** 
 * Controller for the addUser stage
 * @author Andrew Gonzalez
 * @author Andrew Gonzalez
 */
public class AddUserController extends Controller {
	@FXML Button addUserButton;
	@FXML Button cancelButton;
	@FXML TextField userNameTextField;
	@FXML TextField passwordTextField;
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 */
	public void start (Stage primary) {
		this.primary = primary;
	}

	public void addUser () throws IOException {
		String userName = userNameTextField.getText();
		String password = passwordTextField.getText();
		

		//Check if username and password are already listed. If not, add to accounts.txt
		if (!userName.isEmpty() && !password.isEmpty()) {
			try {

				//Need to deserialize storedUsers and then add the user
				FileInputStream fileIn = new FileInputStream("accounts.dat");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
				in.close();
				fileIn.close();
				storedUsers.add(new User(userName, password, "user"));
				
				// Re-serialize storedUsers
				FileOutputStream fileOut = new FileOutputStream("accounts.dat");
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(storedUsers);
				out.close();
				fileOut.close();
				closeWindow();
			}
			catch (ClassNotFoundException ex ) {
			}
			catch (IOException ex) {
			}
		}
		else {
			error("Please provide inputs for both fields"); 
		}
	}
	
}
