package controllers;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.StringTokenizer;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import backend.Album;
import backend.Controller;
import backend.StageManager;
import backend.User;

/** 
 * Controller for the album stage of project
 * @author Daniel Rodriguze
 * @author Andrew Gonzalez
 */
public class AlbumsController extends Controller implements Serializable{

	private static final long serialVersionUID = 1L;

	@FXML ListView<Album> albumsListView;
	@FXML Button add;
	@FXML Button delete;
	@FXML Button edit;
	@FXML Button search;
	@FXML Button logout;
	@FXML Button quit;
	@FXML TextField albumName;
	@FXML TextField numPhotos;
	@FXML TextField dateRange;
	
	private User current;
	private ObservableList<Album> obsList;
	private int num;
	
	public static Comparator<Album> AlbumComparator = new Comparator<Album>() {
		public int compare(Album a1, Album a2) {
			if(a1.getName().compareToIgnoreCase(a2.getName())==0) {
				if(a1.getNewestDateString().compareToIgnoreCase(a2.getNewestDateString())==0){
					return a2.getOldestDateString().compareToIgnoreCase(a1.getOldestDateString());
				}else {	
					return a1.getNewestDateString().compareToIgnoreCase(a2.getNewestDateString());
				}
			}else {
				return a1.getName().compareToIgnoreCase(a2.getName());
			}
		}
	};
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 * @param user is the user accessing stage
	 */
	public void start(Stage primary,User user) {
		this.current = user;
		this.primary = primary;
		displayAlbums();
	}
	
	/**
	 * Open up the addAlbum stage
	 * @param e the event that started from button press 
	 * @throws IOException
	 */
	public void addAlbum (ActionEvent e) throws IOException {
		
		//Second window that makes user enter album info
		StageManager stageManager = new StageManager();
		stageManager.getStage("AddAlbum",current).showAndWait();
		displayAlbums();
	}
	
	
	/**
	 * Opens up the "Edit_Album" stage
	 * @param e the ActionEvent that prompted the button
	 * @throws IOException
	 */
	public void editAlbum(ActionEvent e) throws IOException{
		Album selectedAlbum = albumsListView.getSelectionModel().getSelectedItem();
		
		if (selectedAlbum == null) {
			error("No album selected.");
			return;
		}
		
		// Loads a scene onto a stage
		// Set up EditAlbumController
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/controllers/FXML/EditAlbum.fxml")); //
		Parent root = (Parent)loader.load();
		EditAlbumController controller = loader.getController();
		
		Stage secondaryStage = new Stage();
		controller.start(secondaryStage, current, selectedAlbum);
		
		// Set up secondaryStage
		Scene scene = new Scene(root);
		secondaryStage.setScene(scene);
		secondaryStage.setTitle("Photo App");
		secondaryStage.setResizable(false); 
		secondaryStage.showAndWait();
		
		// After prompting user for edit, re-display updated albums
		displayAlbums();
	}
	
	/**
	 * Open search photo stage for user
	 * @param e the button that started this action
	 * @throws IOException
	 */
	public void searchAlbum(ActionEvent e) throws IOException{
		StageManager stageManager = new StageManager();
		stageManager.loadScene(primary, "SearchGUI", current);
	}
	
	/**
	 * Returns User to the "Login" stage
	 * @param e the ActionEvent that prompted the button
	 * @throws IOException
	 */
	public void logOut (ActionEvent e) throws IOException {
		StageManager stageManager = new StageManager();
		stageManager.loadScene(primary, "LoginGUI");
	}
	
	
	private void displayAlbums () {
		obsList = FXCollections.observableArrayList();
		
		
		// All user albums added to obsList
		for (Album album : current.getAlbums()) {
			obsList.add(album);
		}

		FXCollections.sort(obsList, AlbumComparator);
		
		// Display user albums
		albumsListView.setItems(obsList);
      
		albumsListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {

    	    @Override
    	    public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
    	    	if(newValue!=null) {
    	    		albumName.setText("Album Name: \t"+"\""+newValue.getName()+"\"");
    	    		numPhotos.setText("# of Photos: \t"+newValue.getNumPhoto());
    	    		dateRange.setText("Date Range: \t"+newValue.getOldestDateString()+" - "+newValue.getNewestDateString());
    	    	}
    	    	else {
    	    		albumName.setText("");
    	    		numPhotos.setText("");
    	    		dateRange.setText("");
    	    	}
    	    }
    	});         		
		
		// allows user to double cluck album and retrieve photos in album
		albumsListView.setOnMouseClicked (new EventHandler<MouseEvent>() {
			@Override
			public void handle (MouseEvent click) {
				Album selectedAlbum = albumsListView.getSelectionModel().getSelectedItem();
				if (click.getClickCount() == 2) {
					StageManager stageManager = new StageManager();
					try {
						stageManager.loadPhotosScene(primary, current, selectedAlbum);
					} 
					catch (IOException ex) {
						System.out.println(ex);
					}
				}
			}
		});
		albumsListView.getSelectionModel().select(0);
	}
	
	/**
	 * Deletes album from user's list
	 * @param e the event that started from button press
	 * @throws IOException
	 */
	public void deleteAlbum(ActionEvent e) throws IOException{
		//Retrieves album name and ensures if anything is selected
		Album selectedAlbum = albumsListView.getSelectionModel().getSelectedItem();
		if (selectedAlbum == null) {
			error("No selected album.");
			return;
		}
				
		// Make sure user wants to actually delete album
		StageManager stageManager = new StageManager();

		
		//if (stageManager.getConfirmation()) {
			try {
				// Deserialize storedUsers data
				FileInputStream fileIn = new FileInputStream("accounts.dat");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
				in.close();
				fileIn.close();
						
				// Traverse storedUsers and remove selected album
				for (User u : storedUsers) {
					if (current.equals(u)) {
						current.deleteAlbum(selectedAlbum);
						storedUsers.set(storedUsers.indexOf(u), current);
					}
				}
						
				// Serialize updated storedUsers
				FileOutputStream fileOut = new FileOutputStream("accounts.dat");
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(storedUsers);
				out.close();
				fileOut.close();
			}
			catch (ClassNotFoundException ex) {
				System.out.println("Class not found.");
			}
			catch (IOException ex) {
				System.out.println("Error reading file.");
			}
		//}
		
		displayAlbums();
	}	
}