package controllers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import backend.Album;
import backend.Controller;
import backend.User;

/** 
 * Controller for the edit album stage
 * @author Andrew Gonzalez
 * @author Dan ROdriguez
 */
public class EditAlbumController extends Controller{
	private User current;
	private Album selectedAlbum;
	
	@FXML TextField albumNameTextField;
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 * @param user is the user accessing stage
	 * @param album is the album being passed to controller
	 * @param selectedalbum is the album that will be changed
	 */
	public void start (Stage primary, User current, Album selectedAlbum) {
		this.primary = primary;
		this.current = current;
		this.selectedAlbum = selectedAlbum;
	}
	
	
	//Changes the user's album name to the input of user
	public void editAlbum () {
		String albumName = albumNameTextField.getText();
		selectedAlbum.setName(albumName);
		for (Album album : current.getAlbums()) {
			/*if (album.getName().equals(albumName)) {
				errDialog("Cannot have two albums with the same name.");
			}*/
		}
		
		try {
			// Deserialize storedUsers data and add new User
			FileInputStream fileIn = new FileInputStream("accounts.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
			in.close();
			fileIn.close();
			
			// Traverse storedUsers and remove selected album
			for (User u : storedUsers) {
				if (current.equals(u)) {
					storedUsers.set(storedUsers.indexOf(u), current);
				}
			}
			
			// Serialize updated storedUsers
			FileOutputStream fileOut = new FileOutputStream("accounts.dat");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(storedUsers);
			out.close();
			fileOut.close();
		}
		catch (ClassNotFoundException ex) {
		} 
		catch (FileNotFoundException ex) {
		} 
		catch (IOException ex) {
		}
		
		closeWindow();
	}
}
