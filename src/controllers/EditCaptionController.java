package controllers;

import backend.Controller;
import backend.Photo;
import backend.User;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/** 
 * Controller for the edit caption stage
 * @author Daniel ROdriguez
 * @author Andrew Gonzalez
 */
public class EditCaptionController extends Controller {
	private User current;
	private Photo selectedPhoto;	
	
	@FXML TextField captionTextField;
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 * @param user is the user accessing stage
	 * @param album is the album being passed to controller
	 * @param selectedPhoto is the photo that will be copied
	 */
	public void start (Stage primary, User current, Photo selectedPhoto) {
		this.primary = primary;
		this.current = current;
		this.selectedPhoto = selectedPhoto;
	}

	public void editCaption () {
		String caption = captionTextField.getText();
		selectedPhoto.setCaption(caption);
		current.saveUser();
		closeWindow();
	}
 }
