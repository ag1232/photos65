package controllers;

import java.io.*;
import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import backend.Controller;
import backend.StageManager;
import backend.User;

/** 
 * Controller for the intial login screen
 * @author Andrew Gonzalez
 * @author Daniel Rodriguez
 */
public class LoginController extends Controller{
	@FXML Button signInButton;
	@FXML Button quitButton;
	@FXML TextField userNameTextField;
	@FXML TextField passwordTextField;
	
	private ArrayList<User> users;
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 */
	public void start(Stage primary) {
		this.primary = primary;
	}
	
	/**
	 * Makes sure user is entering correct username + password, and the correct stage is entered
	 * @param e is the event that starts because of a button press 
	 */
	public void checkSignIn (ActionEvent e)  {

		String userName = userNameTextField.getText();
		String password = passwordTextField.getText();
		
		// check txt file of users to check if already present
		User storedUser;
		try {
			FileInputStream fileIn = new FileInputStream("accounts.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			users = (ArrayList<User>) in.readObject();
			boolean validUser = false;
			for (User user : users) {
				if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
					if (user.getAccountType().equals("admin")) {
						StageManager stageManager = new StageManager();
						stageManager.loadScene(primary, "AdminWindow");
						validUser = true;
						break;
					}
					else if (user.getAccountType().equals("user")) {
						StageManager stageManager = new StageManager();
						stageManager.loadScene(primary, "AlbumGUI", user);
						validUser = true;
					}
					
				}
			}
			if (validUser == false) {
				error("Invalid username/password");
			}
			in.close();
		}
		catch (ClassNotFoundException ex) {
		}
		catch (IOException ex) {
		}
	}
}
