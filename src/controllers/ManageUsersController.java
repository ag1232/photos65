package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import javafx.util.Callback;
import backend.Controller;
import backend.StageManager;
import backend.User;

/** 
 * Controls the "Manage_Users" stage
 * @author Andrew Gonzalez
 * @author Daniel Rodriguez
 */
public class ManageUsersController extends Controller{
	private ObservableList<User> users;
	
	@FXML ListView<User> usersListView;
	@FXML Button addButton;
	@FXML Button deleteButton;
	@FXML Button logOutButton;
	@FXML Button quitButton;
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 */
	public void start(Stage primary) {
		displayUsers();
		this.primary = primary;
	}
	
	
	/**
	 * Allows admin to enter the deleteUser stage
	 * @param e the event that is triggered due to a button press 
	 * @throws IOException
	 */
	public void deleteUser (ActionEvent e) throws IOException {
		// Get the selected user's name and check if anything was selected
		User selectedUser = usersListView.getSelectionModel().getSelectedItem();
		if (selectedUser == null) {
			return;
		}

		StageManager stageManager = new StageManager();
		
		//When user verifies, move entire file into temporary file, without the user being deleted
		//if (stageManager.getConfirmation()) {
			try {
				// Deserialize storedUsers data
				FileInputStream fileIn = new FileInputStream("accounts.dat");
				ObjectInputStream in = new ObjectInputStream(fileIn);
				ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
				in.close();
				fileIn.close();
				
				// Traverse storedUsers and remove user that has same credentials as selectedUser
				for (User user : storedUsers) {
					if (user.getUserName().equals(selectedUser.getUserName()) &&
						user.getPassword().equals(selectedUser.getPassword())) {
						storedUsers.remove(user);
						break;
					}
				}

				FileOutputStream fileOut = new FileOutputStream("accounts.dat");
				ObjectOutputStream out = new ObjectOutputStream(fileOut);
				out.writeObject(storedUsers);
				out.close();
				fileOut.close();
			}
			catch (ClassNotFoundException ex) {
			}
			catch (IOException ex) {
			}
		//}
		
		// After deleting a user, re-display to update view
		displayUsers();
	}
	
	/**
	 * Allows admin to enter the addUser stage
	 * @param e the event that is triggered due to a button press 
	 * @throws IOException
	 */
	public void addUser (ActionEvent e) throws IOException {

		StageManager stageManager = new StageManager();
		stageManager.getStage("AddUser").showAndWait();
		
		// Displays the users again after updating 
		displayUsers();
	}
	
	/**
	 * Takes the user back to the login screen
	 * @param e the ActionEvent that prompted the button 
	 * @throws IOException
	 */
	public void logOut (ActionEvent e) throws IOException {
		StageManager stageManager = new StageManager();
		stageManager.loadScene(primary, "LoginGUI");
	}
	

	private void displayUsers () {

		users = FXCollections.observableArrayList();

		// Read "accounts.txt" file and add users to ObservableList
		try {
			FileInputStream fileIn = new FileInputStream("accounts.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
			for (User user : storedUsers) {
				if (user.getAccountType().equals("user")) {
					users.add(user);
				}
			}
			in.close();
		}
		catch (FileNotFoundException ex) {
		}
		catch (IOException ex) {
		} 
		catch (ClassNotFoundException e) {
		}
		
		// Display ObservableList users in usersListView
		usersListView.setItems(users);
		usersListView.setCellFactory(new Callback<ListView<User>, ListCell<User>>(){  
            @Override
            public ListCell<User> call(ListView<User> p) {                 
                ListCell<User> cell = new ListCell<User>(){ 
                    @Override
                    protected void updateItem(User user, boolean bln) {
                        super.updateItem(user, bln);
                        
                        if(user == null){
                        	setText(null);
                        }
                        else {
                        	setText(user.getUserName());
                        }
                    }
 
                };
                 
                return cell;
            }
        });
	}
}
