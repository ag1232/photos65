package controllers;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import backend.Album;
import backend.Controller;
import backend.Photo;
import backend.User;

/** 
 * Controller for the movePhoto stage
 * @author Daniel Rodriguez
 * @author Andy Gonzalez
 */
public class MovePhotoController extends Controller{
	private User current;
	private Album album;
	private Photo selectedPhoto;
	private ObservableList<Album> obsList;
	
	@FXML ListView<Album> albumsListView;
	
	public static Comparator<Album> AlbumComparator = new Comparator<Album>() {
		public int compare(Album a1, Album a2) {
			if(a1.getName().compareToIgnoreCase(a2.getName())==0) {
				if(a1.getNewestDateString().compareToIgnoreCase(a2.getNewestDateString())==0){
					return a2.getOldestDateString().compareToIgnoreCase(a1.getOldestDateString());
				}else {	
					return a1.getNewestDateString().compareToIgnoreCase(a2.getNewestDateString());
				}
			}else {
				return a1.getName().compareToIgnoreCase(a2.getName());
			}
		}
	};	
	
	/**
	 * Initializes controller fields
	 * @param primary is the stage attached to the controller
	 * @param user is the user accessing stage
	 * @param album is the album being passed to controller
	 * @param selectedPhoto is the photo that will be moved
	 */
	public void start (Stage primary, User current, Album album, Photo selectedPhoto) {
		this.primary = primary;
		this.current = current;
		this.album = album;
		this.selectedPhoto = selectedPhoto;
		
		displayAlbums();
	}

	public void movePhoto () {
		Album selectedAlbum = albumsListView.getSelectionModel().getSelectedItem();
		if (selectedAlbum == null) {
			return;
		}
		
		album.deletePhoto(selectedPhoto);
		selectedAlbum.addPhoto(selectedPhoto);
		
		try {
			// Deserialize storedUsers data
			FileInputStream fileIn = new FileInputStream("accounts.dat");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			ArrayList<User> storedUsers = (ArrayList<User>) in.readObject();
			in.close();
			fileIn.close();
					
			// Traverse storedUsers and remove selected album
			for (User u : storedUsers) {
				if (current.equals(u)) {
					storedUsers.set(storedUsers.indexOf(u), current);
				}
			}
					
			// Serialize updated storedUsers
			FileOutputStream fileOut = new FileOutputStream("accounts.dat");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);
			out.writeObject(storedUsers);
			out.close();
			fileOut.close();
		}
		catch (ClassNotFoundException ex) {
		}
		catch (IOException ex) {
		}
		
		closeWindow();
	}

	private void displayAlbums () {
		obsList = FXCollections.observableArrayList();
		for (Album album : current.getAlbums()) {
			if (album != this.album) {
				obsList.add(album);
			}
		}

		FXCollections.sort(obsList, AlbumComparator);
		
		albumsListView.setItems(obsList);
		albumsListView.setCellFactory(param -> new ListCell<Album>() {
			@Override
			public void updateItem (Album album, boolean empty) {
				super.updateItem(album, empty);
				
				if(!empty){
					setText(album.getName());
				}
				else{
					setText(null);
				}
			}
		});		
	}
}
