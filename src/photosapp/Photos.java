package photosapp;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import backend.StageManager;
import backend.User;
import javafx.application.Application;
import javafx.stage.Stage;

public class Photos extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		StageManager stageManager = new StageManager();
		stageManager.loadScene(primaryStage, "LoginGUI");
	}
	public static void main (String [] args){
		launch(args);
	}
	
}
